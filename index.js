import puppeteer from "puppeteer";

(async () => {
  const URL_WITH_USE_FORM =
    "http://localhost:9001/iframe.html?args=&id=hooks-useform--default&viewMode=story";
  const URL_WITHOUT_USE_FORM =
    "http://localhost:9001/iframe.html?args=&id=hooks-useform--not-using-use-form&viewMode=story";

  const WITH_USE_FORM = true;

  const createTimings = [];
  const updateTimings = [];

  for (let index = 0; index < 100; index++) {
    const browser = await puppeteer.launch({
      headless: false,
      executablePath: "/opt/homebrew/bin/chromium",
    });
    const page = await browser.newPage();

    await page.goto(WITH_USE_FORM ? URL_WITH_USE_FORM : URL_WITHOUT_USE_FORM);

    await page.waitForSelector('button[type="submit"]');

    await page.click('button[type="submit"]');

    const perfEntries = JSON.parse(
      await page.evaluate(() => JSON.stringify(performance.getEntries()))
    );

    createTimings.push(
      perfEntries.find((x) => x.name === "create-duration").duration
    );
    updateTimings.push(
      perfEntries.find((x) => x.name === "update-duration").duration
    );

    await browser.close();
  }

  console.log(
    `Create average: ${
      createTimings.reduce((a, b) => a + b) / createTimings.length
    }`
  );
  console.log(
    `Update average: ${
      updateTimings.reduce((a, b) => a + b) / updateTimings.length
    }`
  );
})();
