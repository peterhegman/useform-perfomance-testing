## Background

This script is used to test performance of the `useForm` hook proposed in https://gitlab.com/gitlab-org/gitlab-ui/-/issues/2126.

It uses puppeteer to open up the example, submit the form, and then use User Timing API to record how long it took to initially render the component and update the component.

## How to use?

1. Checkout https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/3232
1. In the GitLab UI directory run `yarn storybook`.
1. In this directory run `yarn install`
1. Update the `WITH_USE_FORM` constant to test with or without `useForm` hook.
1. Run `node ./index.js`
1. Compare the averages
